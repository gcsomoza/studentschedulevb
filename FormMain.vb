﻿Imports System.IO

Public Class FormMain

    Private Sub ClearStudentInfo(ByVal msg As String, ByVal clr As Color)
        lblMsg.Text = msg
        lblMsg.ForeColor = clr

        lblName.Text = "-"
        lblSection.Text = "-"
        pbImage.BackgroundImage = My.Resources.no_image_available

        lblSchedule.Text = "-"
        lblLunch.Text = "-"
    End Sub

    Private Sub ValidateStudent()

        'Makes the timer tick only once
        'If removed it will crash the program.
        'Beware!
        Timer1.Stop()

        Dim student As New Student
        Dim studentID As Integer
        Dim dayOfWeek As Integer = DateTime.Now.DayOfWeek
        Dim timeStart As Integer
        Dim timeEnd As Integer
        Dim lunchStart As Integer
        Dim lunchEnd As Integer

        If Not Me.txtBarCode.Text = "" Then
            studentID = student.GetID(Me.txtBarCode.Text.TrimStart("0"c))
            If studentID > 0 Then
                timeStart = student.GetTimeStart(studentID, dayOfWeek)
                timeEnd = student.GetTimeEnd(studentID, dayOfWeek)
                lunchStart = student.GetLunchStart(studentID, dayOfWeek)
                lunchEnd = student.GetLunchEnd(studentID, dayOfWeek)

                'Student found.
                'Check if he/she can exit
                If CanExit(timeStart, timeEnd, lunchStart, lunchEnd) Then
                    lblMsg.Text = "Let him go!"
                    lblMsg.ForeColor = Color.Green
                Else
                    lblMsg.Text = "Stop him!"
                    lblMsg.ForeColor = Color.Red
                End If

                'Display student information
                student.id = studentID
                student.GetStudentData()

                'Display Student Image
                If Not student.img Is Nothing Then
                    Using ms As New MemoryStream(student.img, 0, student.img.Length)
                        ms.Write(student.img, 0, student.img.Length)
                        pbImage.BackgroundImage = Image.FromStream(ms, True)
                    End Using
                End If

                lblName.Text = student.first_name + " " + student.middle_name + " " + student.last_name
                If student.section = "" Then
                    lblSection.Text = student.section
                Else
                    lblSection.Text = "N/A"
                End If

                'Display Student Today's Schedule
                lblSchedule.Text = FormatTime(timeStart) + " to " + FormatTime(timeEnd)
                If lunchStart > 0 And lunchEnd > 0 Then
                    lblLunch.Text = FormatTime(lunchStart) + " to " + FormatTime(lunchEnd)
                Else
                    lblLunch.Text = "N/A"
                End If

            Else
                ClearStudentInfo("Student not found!", Color.Red)
            End If
        Else
            ClearStudentInfo("Enter Student ID or Barcode", Color.Blue)
        End If
    End Sub

    Private Sub btnValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValidate.Click
        ValidateStudent()
    End Sub

    Private Sub DatabaseConnectionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DatabaseConnectionToolStripMenuItem.Click
        Dim frm As New FormDatabaseConnection()
        frm.Show()
    End Sub

    Private Sub StudentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StudentToolStripMenuItem.Click
        Dim frm As New FormStudentList()
        frm.Show()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'Prevents the Tick events from being
        'processed after the timer was stopped
        If Not Timer1.Enabled Then Return

        ValidateStudent()
    End Sub

    Private Sub txtBarCode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBarCode.TextChanged
        Timer1.Stop()
        Timer1.Start()
    End Sub

    Private Sub FormMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtBarCode.Focus()
        ClearStudentInfo("Enter Student ID or Barcode", Color.Blue)
    End Sub
End Class