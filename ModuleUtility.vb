﻿Imports System.IO
Imports MySql.Data.MySqlClient

Public Module Utility

    ' Get the connections string from the configuration file in My Documents folder.
    Public Function GetConnectionString() As String
        Dim connectionString As String

        Dim filePath As String
        filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\", "StudentScheduleConnectionString.txt")

        Try
            Using reader As New StreamReader(filePath)
                connectionString = reader.ReadLine()
            End Using

        Catch ex As FileNotFoundException
            connectionString = ""

        End Try

        Return connectionString

    End Function

    ' Time is in military format.
    ' In case that you want 2:00PM then you enter 1400.
    ' In case that you want 9:00PM then you enter 900.
    ' In case that you want 11:30PM then you enter 1130.
    '
    ' Enter negative number for lunchStart and lunchEnd if the student has no lunch break.
    Public Function CanExit(ByVal timeStart As Integer, ByVal timeEnd As Integer, ByVal lunchStart As Integer, ByVal lunchEnd As Integer) As Boolean

        ' Get the current time.
        ' This is dependent on the time of computer.
        ' If the time of the computer is wrong then you must correct it.
        Dim currentTime As Integer
        currentTime = Integer.Parse(DateTime.Now.ToString("HHmm"))

        ' The student enters the school too early but 
        ' the student suddenly wants to go outside then 
        ' the student should be able to go outside.
        If currentTime < timeStart Then
            Return True
        End If

        ' The student can go outside at within the time of his lunch break.
        If lunchStart > 0 And lunchEnd > 0 Then
            If currentTime >= lunchStart And currentTime < lunchEnd Then
                Return True
            End If
        End If

        ' The student can only go outside after his schedule.
        If currentTime > timeEnd Then
            Return True
        End If

        Return False
    End Function

    Public Function FormatTime(ByVal intTime As Integer) As String
        Dim strTime As String
        Dim hh As Integer
        Dim mm As Integer
        Dim dt As Date

        hh = intTime / 100
        mm = intTime Mod 100
        dt = New Date(1, 1, 1, hh, mm, 0)
        strTime = dt.ToString("hh:mmtt")

        Return strTime
    End Function

End Module
