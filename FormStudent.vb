﻿Imports System.IO
Imports System.Drawing.Imaging

Public Class FormStudent

    Public id As Integer
    Public frmCaller As FormStudentList

    Private Sub FormStudent_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Me.id > 0 Then
            'Load Student
            Dim student As New Student()
            student.id = Me.id
            student.GetStudentData()

            Me.student_id.Text = student.student_id
            Me.first_name.Text = student.first_name
            Me.middle_name.Text = student.middle_name
            Me.last_name.Text = student.last_name
            Me.grade_level.Text = student.grade_level
            Me.section.Text = student.section

            'Load Student Image
            If Not student.img Is Nothing Then
                Using ms As New MemoryStream(student.img, 0, student.img.Length)
                    ms.Write(student.img, 0, student.img.Length)
                    studentImage.BackgroundImage = Image.FromStream(ms, True)
                End Using
            End If

            'Load Student Schedule
            Dim n As Date = Now
            Dim time As Integer
            Dim hour As Integer
            Dim minute As Integer

            'Time Start
            time = student.GetTimeStart(student.id, 1)
            hour = time / 100
            minute = time Mod 100
            Me.dtTimeStart1.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetTimeStart(student.id, 2)
            hour = time / 100
            minute = time Mod 100
            Me.dtTimeStart2.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetTimeStart(student.id, 3)
            hour = time / 100
            minute = time Mod 100
            Me.dtTimeStart3.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetTimeStart(student.id, 4)
            hour = time / 100
            minute = time Mod 100
            Me.dtTimeStart4.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetTimeStart(student.id, 5)
            hour = time / 100
            minute = time Mod 100
            Me.dtTimeStart5.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetTimeStart(student.id, 6)
            hour = time / 100
            minute = time Mod 100
            Me.dtTimeStart6.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            'Lunch Start
            time = student.GetLunchStart(student.id, 1)
            hour = time / 100
            minute = time Mod 100
            Me.dtLunchStart1.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetLunchStart(student.id, 2)
            hour = time / 100
            minute = time Mod 100
            Me.dtLunchStart2.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetLunchStart(student.id, 3)
            hour = time / 100
            minute = time Mod 100
            Me.dtLunchStart3.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetLunchStart(student.id, 4)
            hour = time / 100
            minute = time Mod 100
            Me.dtLunchStart4.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetLunchStart(student.id, 5)
            hour = time / 100
            minute = time Mod 100
            Me.dtLunchStart5.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetLunchStart(student.id, 6)
            hour = time / 100
            minute = time Mod 100
            Me.dtLunchStart6.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            'Lunch End
            time = student.GetLunchEnd(student.id, 1)
            hour = time / 100
            minute = time Mod 100
            Me.dtLunchEnd1.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetLunchEnd(student.id, 2)
            hour = time / 100
            minute = time Mod 100
            Me.dtLunchEnd2.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetLunchEnd(student.id, 3)
            hour = time / 100
            minute = time Mod 100
            Me.dtLunchEnd3.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetLunchEnd(student.id, 4)
            hour = time / 100
            minute = time Mod 100
            Me.dtLunchEnd4.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetLunchEnd(student.id, 5)
            hour = time / 100
            minute = time Mod 100
            Me.dtLunchEnd5.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetLunchEnd(student.id, 6)
            hour = time / 100
            minute = time Mod 100
            Me.dtLunchEnd6.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            'Time End
            time = student.GetTimeEnd(student.id, 1)
            hour = time / 100
            minute = time Mod 100
            Me.dtTimeEnd1.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetTimeEnd(student.id, 2)
            hour = time / 100
            minute = time Mod 100
            Me.dtTimeEnd2.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetTimeEnd(student.id, 3)
            hour = time / 100
            minute = time Mod 100
            Me.dtTimeEnd3.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetTimeEnd(student.id, 4)
            hour = time / 100
            minute = time Mod 100
            Me.dtTimeEnd4.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetTimeEnd(student.id, 5)
            hour = time / 100
            minute = time Mod 100
            Me.dtTimeEnd5.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            time = student.GetTimeEnd(student.id, 6)
            hour = time / 100
            minute = time Mod 100
            Me.dtTimeEnd6.Value = New Date(n.Year, n.Month, n.Day, hour, minute, 0)

            Me.btnDelete.Enabled = True
        Else
            'Load No Image Available
            studentImage.BackgroundImage = My.Resources.no_image_available
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim msg As String = "Student successfully updated!"

        If Me.id = 0 Then
            'Add Student
            Dim student As New Student()

            student.id = Me.id
            student.student_id = Me.student_id.Text
            student.bar_code = Me.student_id.Text
            student.first_name = Me.first_name.Text
            student.middle_name = Me.middle_name.Text
            student.last_name = Me.last_name.Text
            student.grade_level = Me.grade_level.Text
            student.section = Me.section.Text

            Dim ms As New MemoryStream()
            studentImage.BackgroundImage.Save(ms, ImageFormat.Png)
            Dim img(ms.Length) As Byte
            ms.Position = 0
            ms.Read(img, 0, img.Length)
            student.img = img

            Me.id = student.Insert()

            msg = "Student successfully added!"

            student.SaveSchedule(1, Integer.Parse(dtTimeStart1.Value.ToString("HHmm")), Integer.Parse(dtLunchStart1.Value.ToString("HHmm")), Integer.Parse(dtLunchEnd1.Value.ToString("HHmm")), Integer.Parse(dtTimeEnd1.Value.ToString("HHmm")))
            student.SaveSchedule(2, Integer.Parse(dtTimeStart2.Value.ToString("HHmm")), Integer.Parse(dtLunchStart2.Value.ToString("HHmm")), Integer.Parse(dtLunchEnd2.Value.ToString("HHmm")), Integer.Parse(dtTimeEnd2.Value.ToString("HHmm")))
            student.SaveSchedule(3, Integer.Parse(dtTimeStart3.Value.ToString("HHmm")), Integer.Parse(dtLunchStart3.Value.ToString("HHmm")), Integer.Parse(dtLunchEnd3.Value.ToString("HHmm")), Integer.Parse(dtTimeEnd3.Value.ToString("HHmm")))
            student.SaveSchedule(4, Integer.Parse(dtTimeStart4.Value.ToString("HHmm")), Integer.Parse(dtLunchStart4.Value.ToString("HHmm")), Integer.Parse(dtLunchEnd4.Value.ToString("HHmm")), Integer.Parse(dtTimeEnd4.Value.ToString("HHmm")))
            student.SaveSchedule(5, Integer.Parse(dtTimeStart5.Value.ToString("HHmm")), Integer.Parse(dtLunchStart5.Value.ToString("HHmm")), Integer.Parse(dtLunchEnd5.Value.ToString("HHmm")), Integer.Parse(dtTimeEnd5.Value.ToString("HHmm")))
            student.SaveSchedule(6, Integer.Parse(dtTimeStart6.Value.ToString("HHmm")), Integer.Parse(dtLunchStart6.Value.ToString("HHmm")), Integer.Parse(dtLunchEnd6.Value.ToString("HHmm")), Integer.Parse(dtTimeEnd6.Value.ToString("HHmm")))

            Me.btnDelete.Enabled = True
        Else
            'Update Student
            Dim student As New Student()

            student.id = Me.id
            student.student_id = Me.student_id.Text
            student.bar_code = Me.student_id.Text
            student.first_name = Me.first_name.Text
            student.middle_name = Me.middle_name.Text
            student.last_name = Me.last_name.Text
            student.grade_level = Me.grade_level.Text
            student.section = Me.section.Text

            Dim ms As New MemoryStream()
            studentImage.BackgroundImage.Save(ms, ImageFormat.Png)
            Dim img(ms.Length) As Byte
            ms.Position = 0
            ms.Read(img, 0, img.Length)
            student.img = img

            student.Update()

            student.SaveSchedule(1, Integer.Parse(dtTimeStart1.Value.ToString("HHmm")), Integer.Parse(dtLunchStart1.Value.ToString("HHmm")), Integer.Parse(dtLunchEnd1.Value.ToString("HHmm")), Integer.Parse(dtTimeEnd1.Value.ToString("HHmm")))
            student.SaveSchedule(2, Integer.Parse(dtTimeStart2.Value.ToString("HHmm")), Integer.Parse(dtLunchStart2.Value.ToString("HHmm")), Integer.Parse(dtLunchEnd2.Value.ToString("HHmm")), Integer.Parse(dtTimeEnd2.Value.ToString("HHmm")))
            student.SaveSchedule(3, Integer.Parse(dtTimeStart3.Value.ToString("HHmm")), Integer.Parse(dtLunchStart3.Value.ToString("HHmm")), Integer.Parse(dtLunchEnd3.Value.ToString("HHmm")), Integer.Parse(dtTimeEnd3.Value.ToString("HHmm")))
            student.SaveSchedule(4, Integer.Parse(dtTimeStart4.Value.ToString("HHmm")), Integer.Parse(dtLunchStart4.Value.ToString("HHmm")), Integer.Parse(dtLunchEnd4.Value.ToString("HHmm")), Integer.Parse(dtTimeEnd4.Value.ToString("HHmm")))
            student.SaveSchedule(5, Integer.Parse(dtTimeStart5.Value.ToString("HHmm")), Integer.Parse(dtLunchStart5.Value.ToString("HHmm")), Integer.Parse(dtLunchEnd5.Value.ToString("HHmm")), Integer.Parse(dtTimeEnd5.Value.ToString("HHmm")))
            student.SaveSchedule(6, Integer.Parse(dtTimeStart6.Value.ToString("HHmm")), Integer.Parse(dtLunchStart6.Value.ToString("HHmm")), Integer.Parse(dtLunchEnd6.Value.ToString("HHmm")), Integer.Parse(dtTimeEnd6.Value.ToString("HHmm")))
        End If

        MessageBox.Show(msg, "Student", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Me.id > 0 Then
            Dim msgboxResult As Integer
            msgboxResult = MessageBox.Show("Delete student?", "Student", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If msgboxResult = DialogResult.Yes Then
                'Delete Student
                Dim student As New Student()
                student.id = Me.id
                student.Delete()
            End If

            MessageBox.Show("Student deleted!", "Student", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()
        Else
            MessageBox.Show("No student to delete!", "Student", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub FormStudent_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If Not frmCaller Is Nothing Then
            frmCaller.LoadStudents("")
        End If
    End Sub

    Private Sub image_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles studentImage.DoubleClick

        OpenFileDialog1.FileName = ""

        If OpenFileDialog1.ShowDialog(Me) = DialogResult.OK Then
            Dim img As String = OpenFileDialog1.FileName

            studentImage.BackgroundImage = System.Drawing.Bitmap.FromFile(img)
            studentImage.BackgroundImageLayout = ImageLayout.Stretch
        End If
    End Sub
End Class