﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormStudent
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.first_name = New System.Windows.Forms.TextBox
        Me.middle_name = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.last_name = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.student_id = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.section = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.grade_level = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.studentImage = New System.Windows.Forms.PictureBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dtTimeEnd6 = New System.Windows.Forms.DateTimePicker
        Me.dtLunchEnd6 = New System.Windows.Forms.DateTimePicker
        Me.dtLunchStart6 = New System.Windows.Forms.DateTimePicker
        Me.dtTimeStart6 = New System.Windows.Forms.DateTimePicker
        Me.dtTimeEnd5 = New System.Windows.Forms.DateTimePicker
        Me.dtLunchEnd5 = New System.Windows.Forms.DateTimePicker
        Me.dtLunchStart5 = New System.Windows.Forms.DateTimePicker
        Me.dtTimeStart5 = New System.Windows.Forms.DateTimePicker
        Me.dtTimeEnd4 = New System.Windows.Forms.DateTimePicker
        Me.dtLunchEnd4 = New System.Windows.Forms.DateTimePicker
        Me.dtLunchStart4 = New System.Windows.Forms.DateTimePicker
        Me.dtTimeStart4 = New System.Windows.Forms.DateTimePicker
        Me.dtTimeEnd3 = New System.Windows.Forms.DateTimePicker
        Me.dtLunchEnd3 = New System.Windows.Forms.DateTimePicker
        Me.dtLunchStart3 = New System.Windows.Forms.DateTimePicker
        Me.dtTimeStart3 = New System.Windows.Forms.DateTimePicker
        Me.dtTimeEnd2 = New System.Windows.Forms.DateTimePicker
        Me.dtLunchEnd2 = New System.Windows.Forms.DateTimePicker
        Me.dtLunchStart2 = New System.Windows.Forms.DateTimePicker
        Me.dtTimeStart2 = New System.Windows.Forms.DateTimePicker
        Me.dtTimeEnd1 = New System.Windows.Forms.DateTimePicker
        Me.dtLunchEnd1 = New System.Windows.Forms.DateTimePicker
        Me.dtLunchStart1 = New System.Windows.Forms.DateTimePicker
        Me.dtTimeStart1 = New System.Windows.Forms.DateTimePicker
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        CType(Me.studentImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 48)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Firtst Name"
        '
        'first_name
        '
        Me.first_name.Location = New System.Drawing.Point(15, 64)
        Me.first_name.Name = "first_name"
        Me.first_name.Size = New System.Drawing.Size(137, 20)
        Me.first_name.TabIndex = 2
        '
        'middle_name
        '
        Me.middle_name.Location = New System.Drawing.Point(159, 64)
        Me.middle_name.Name = "middle_name"
        Me.middle_name.Size = New System.Drawing.Size(137, 20)
        Me.middle_name.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(156, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Middle Name"
        '
        'last_name
        '
        Me.last_name.Location = New System.Drawing.Point(305, 64)
        Me.last_name.Name = "last_name"
        Me.last_name.Size = New System.Drawing.Size(137, 20)
        Me.last_name.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(302, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Last Name"
        '
        'student_id
        '
        Me.student_id.Location = New System.Drawing.Point(15, 25)
        Me.student_id.Name = "student_id"
        Me.student_id.Size = New System.Drawing.Size(137, 20)
        Me.student_id.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Student ID"
        '
        'section
        '
        Me.section.Location = New System.Drawing.Point(161, 103)
        Me.section.Name = "section"
        Me.section.Size = New System.Drawing.Size(137, 20)
        Me.section.TabIndex = 6
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(158, 87)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Section"
        '
        'grade_level
        '
        Me.grade_level.Location = New System.Drawing.Point(15, 103)
        Me.grade_level.Name = "grade_level"
        Me.grade_level.Size = New System.Drawing.Size(137, 20)
        Me.grade_level.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 87)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Grade Level"
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(19, 405)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Enabled = False
        Me.btnDelete.Location = New System.Drawing.Point(100, 405)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 23)
        Me.btnDelete.TabIndex = 8
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(181, 405)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 9
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'studentImage
        '
        Me.studentImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.studentImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.studentImage.Location = New System.Drawing.Point(494, 25)
        Me.studentImage.Name = "studentImage"
        Me.studentImage.Size = New System.Drawing.Size(139, 111)
        Me.studentImage.TabIndex = 15
        Me.studentImage.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.dtTimeEnd6)
        Me.Panel1.Controls.Add(Me.dtLunchEnd6)
        Me.Panel1.Controls.Add(Me.dtLunchStart6)
        Me.Panel1.Controls.Add(Me.dtTimeStart6)
        Me.Panel1.Controls.Add(Me.dtTimeEnd5)
        Me.Panel1.Controls.Add(Me.dtLunchEnd5)
        Me.Panel1.Controls.Add(Me.dtLunchStart5)
        Me.Panel1.Controls.Add(Me.dtTimeStart5)
        Me.Panel1.Controls.Add(Me.dtTimeEnd4)
        Me.Panel1.Controls.Add(Me.dtLunchEnd4)
        Me.Panel1.Controls.Add(Me.dtLunchStart4)
        Me.Panel1.Controls.Add(Me.dtTimeStart4)
        Me.Panel1.Controls.Add(Me.dtTimeEnd3)
        Me.Panel1.Controls.Add(Me.dtLunchEnd3)
        Me.Panel1.Controls.Add(Me.dtLunchStart3)
        Me.Panel1.Controls.Add(Me.dtTimeStart3)
        Me.Panel1.Controls.Add(Me.dtTimeEnd2)
        Me.Panel1.Controls.Add(Me.dtLunchEnd2)
        Me.Panel1.Controls.Add(Me.dtLunchStart2)
        Me.Panel1.Controls.Add(Me.dtTimeStart2)
        Me.Panel1.Controls.Add(Me.dtTimeEnd1)
        Me.Panel1.Controls.Add(Me.dtLunchEnd1)
        Me.Panel1.Controls.Add(Me.dtLunchStart1)
        Me.Panel1.Controls.Add(Me.dtTimeStart1)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Location = New System.Drawing.Point(20, 158)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(640, 241)
        Me.Panel1.TabIndex = 16
        '
        'dtTimeEnd6
        '
        Me.dtTimeEnd6.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtTimeEnd6.Location = New System.Drawing.Point(401, 180)
        Me.dtTimeEnd6.Name = "dtTimeEnd6"
        Me.dtTimeEnd6.ShowUpDown = True
        Me.dtTimeEnd6.Size = New System.Drawing.Size(101, 20)
        Me.dtTimeEnd6.TabIndex = 58
        Me.dtTimeEnd6.Value = New Date(2018, 2, 10, 18, 0, 0, 0)
        '
        'dtLunchEnd6
        '
        Me.dtLunchEnd6.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtLunchEnd6.Location = New System.Drawing.Point(294, 180)
        Me.dtLunchEnd6.Name = "dtLunchEnd6"
        Me.dtLunchEnd6.ShowUpDown = True
        Me.dtLunchEnd6.Size = New System.Drawing.Size(101, 20)
        Me.dtLunchEnd6.TabIndex = 57
        Me.dtLunchEnd6.Value = New Date(2018, 2, 10, 13, 0, 0, 0)
        '
        'dtLunchStart6
        '
        Me.dtLunchStart6.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtLunchStart6.Location = New System.Drawing.Point(187, 180)
        Me.dtLunchStart6.Name = "dtLunchStart6"
        Me.dtLunchStart6.ShowUpDown = True
        Me.dtLunchStart6.Size = New System.Drawing.Size(101, 20)
        Me.dtLunchStart6.TabIndex = 56
        Me.dtLunchStart6.Value = New Date(2018, 2, 10, 12, 0, 0, 0)
        '
        'dtTimeStart6
        '
        Me.dtTimeStart6.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtTimeStart6.Location = New System.Drawing.Point(80, 180)
        Me.dtTimeStart6.Name = "dtTimeStart6"
        Me.dtTimeStart6.ShowUpDown = True
        Me.dtTimeStart6.Size = New System.Drawing.Size(101, 20)
        Me.dtTimeStart6.TabIndex = 55
        Me.dtTimeStart6.Value = New Date(2018, 2, 10, 6, 0, 0, 0)
        '
        'dtTimeEnd5
        '
        Me.dtTimeEnd5.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtTimeEnd5.Location = New System.Drawing.Point(401, 154)
        Me.dtTimeEnd5.Name = "dtTimeEnd5"
        Me.dtTimeEnd5.ShowUpDown = True
        Me.dtTimeEnd5.Size = New System.Drawing.Size(101, 20)
        Me.dtTimeEnd5.TabIndex = 54
        Me.dtTimeEnd5.Value = New Date(2018, 2, 10, 18, 0, 0, 0)
        '
        'dtLunchEnd5
        '
        Me.dtLunchEnd5.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtLunchEnd5.Location = New System.Drawing.Point(294, 154)
        Me.dtLunchEnd5.Name = "dtLunchEnd5"
        Me.dtLunchEnd5.ShowUpDown = True
        Me.dtLunchEnd5.Size = New System.Drawing.Size(101, 20)
        Me.dtLunchEnd5.TabIndex = 53
        Me.dtLunchEnd5.Value = New Date(2018, 2, 10, 13, 0, 0, 0)
        '
        'dtLunchStart5
        '
        Me.dtLunchStart5.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtLunchStart5.Location = New System.Drawing.Point(187, 154)
        Me.dtLunchStart5.Name = "dtLunchStart5"
        Me.dtLunchStart5.ShowUpDown = True
        Me.dtLunchStart5.Size = New System.Drawing.Size(101, 20)
        Me.dtLunchStart5.TabIndex = 52
        Me.dtLunchStart5.Value = New Date(2018, 2, 10, 12, 0, 0, 0)
        '
        'dtTimeStart5
        '
        Me.dtTimeStart5.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtTimeStart5.Location = New System.Drawing.Point(80, 154)
        Me.dtTimeStart5.Name = "dtTimeStart5"
        Me.dtTimeStart5.ShowUpDown = True
        Me.dtTimeStart5.Size = New System.Drawing.Size(101, 20)
        Me.dtTimeStart5.TabIndex = 51
        Me.dtTimeStart5.Value = New Date(2018, 2, 10, 6, 0, 0, 0)
        '
        'dtTimeEnd4
        '
        Me.dtTimeEnd4.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtTimeEnd4.Location = New System.Drawing.Point(401, 128)
        Me.dtTimeEnd4.Name = "dtTimeEnd4"
        Me.dtTimeEnd4.ShowUpDown = True
        Me.dtTimeEnd4.Size = New System.Drawing.Size(101, 20)
        Me.dtTimeEnd4.TabIndex = 50
        Me.dtTimeEnd4.Value = New Date(2018, 2, 10, 18, 0, 0, 0)
        '
        'dtLunchEnd4
        '
        Me.dtLunchEnd4.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtLunchEnd4.Location = New System.Drawing.Point(294, 128)
        Me.dtLunchEnd4.Name = "dtLunchEnd4"
        Me.dtLunchEnd4.ShowUpDown = True
        Me.dtLunchEnd4.Size = New System.Drawing.Size(101, 20)
        Me.dtLunchEnd4.TabIndex = 49
        Me.dtLunchEnd4.Value = New Date(2018, 2, 10, 13, 0, 0, 0)
        '
        'dtLunchStart4
        '
        Me.dtLunchStart4.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtLunchStart4.Location = New System.Drawing.Point(187, 128)
        Me.dtLunchStart4.Name = "dtLunchStart4"
        Me.dtLunchStart4.ShowUpDown = True
        Me.dtLunchStart4.Size = New System.Drawing.Size(101, 20)
        Me.dtLunchStart4.TabIndex = 48
        Me.dtLunchStart4.Value = New Date(2018, 2, 10, 12, 0, 0, 0)
        '
        'dtTimeStart4
        '
        Me.dtTimeStart4.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtTimeStart4.Location = New System.Drawing.Point(80, 128)
        Me.dtTimeStart4.Name = "dtTimeStart4"
        Me.dtTimeStart4.ShowUpDown = True
        Me.dtTimeStart4.Size = New System.Drawing.Size(101, 20)
        Me.dtTimeStart4.TabIndex = 47
        Me.dtTimeStart4.Value = New Date(2018, 2, 10, 6, 0, 0, 0)
        '
        'dtTimeEnd3
        '
        Me.dtTimeEnd3.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtTimeEnd3.Location = New System.Drawing.Point(401, 102)
        Me.dtTimeEnd3.Name = "dtTimeEnd3"
        Me.dtTimeEnd3.ShowUpDown = True
        Me.dtTimeEnd3.Size = New System.Drawing.Size(101, 20)
        Me.dtTimeEnd3.TabIndex = 46
        Me.dtTimeEnd3.Value = New Date(2018, 2, 10, 18, 0, 0, 0)
        '
        'dtLunchEnd3
        '
        Me.dtLunchEnd3.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtLunchEnd3.Location = New System.Drawing.Point(294, 102)
        Me.dtLunchEnd3.Name = "dtLunchEnd3"
        Me.dtLunchEnd3.ShowUpDown = True
        Me.dtLunchEnd3.Size = New System.Drawing.Size(101, 20)
        Me.dtLunchEnd3.TabIndex = 45
        Me.dtLunchEnd3.Value = New Date(2018, 2, 10, 13, 0, 0, 0)
        '
        'dtLunchStart3
        '
        Me.dtLunchStart3.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtLunchStart3.Location = New System.Drawing.Point(187, 102)
        Me.dtLunchStart3.Name = "dtLunchStart3"
        Me.dtLunchStart3.ShowUpDown = True
        Me.dtLunchStart3.Size = New System.Drawing.Size(101, 20)
        Me.dtLunchStart3.TabIndex = 44
        Me.dtLunchStart3.Value = New Date(2018, 2, 10, 12, 0, 0, 0)
        '
        'dtTimeStart3
        '
        Me.dtTimeStart3.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtTimeStart3.Location = New System.Drawing.Point(80, 102)
        Me.dtTimeStart3.Name = "dtTimeStart3"
        Me.dtTimeStart3.ShowUpDown = True
        Me.dtTimeStart3.Size = New System.Drawing.Size(101, 20)
        Me.dtTimeStart3.TabIndex = 43
        Me.dtTimeStart3.Value = New Date(2018, 2, 10, 6, 0, 0, 0)
        '
        'dtTimeEnd2
        '
        Me.dtTimeEnd2.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtTimeEnd2.Location = New System.Drawing.Point(401, 79)
        Me.dtTimeEnd2.Name = "dtTimeEnd2"
        Me.dtTimeEnd2.ShowUpDown = True
        Me.dtTimeEnd2.Size = New System.Drawing.Size(101, 20)
        Me.dtTimeEnd2.TabIndex = 42
        Me.dtTimeEnd2.Value = New Date(2018, 2, 10, 18, 0, 0, 0)
        '
        'dtLunchEnd2
        '
        Me.dtLunchEnd2.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtLunchEnd2.Location = New System.Drawing.Point(294, 79)
        Me.dtLunchEnd2.Name = "dtLunchEnd2"
        Me.dtLunchEnd2.ShowUpDown = True
        Me.dtLunchEnd2.Size = New System.Drawing.Size(101, 20)
        Me.dtLunchEnd2.TabIndex = 41
        Me.dtLunchEnd2.Value = New Date(2018, 2, 10, 13, 0, 0, 0)
        '
        'dtLunchStart2
        '
        Me.dtLunchStart2.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtLunchStart2.Location = New System.Drawing.Point(187, 79)
        Me.dtLunchStart2.Name = "dtLunchStart2"
        Me.dtLunchStart2.ShowUpDown = True
        Me.dtLunchStart2.Size = New System.Drawing.Size(101, 20)
        Me.dtLunchStart2.TabIndex = 40
        Me.dtLunchStart2.Value = New Date(2018, 2, 10, 12, 0, 0, 0)
        '
        'dtTimeStart2
        '
        Me.dtTimeStart2.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtTimeStart2.Location = New System.Drawing.Point(80, 79)
        Me.dtTimeStart2.Name = "dtTimeStart2"
        Me.dtTimeStart2.ShowUpDown = True
        Me.dtTimeStart2.Size = New System.Drawing.Size(101, 20)
        Me.dtTimeStart2.TabIndex = 39
        Me.dtTimeStart2.Value = New Date(2018, 2, 10, 6, 0, 0, 0)
        '
        'dtTimeEnd1
        '
        Me.dtTimeEnd1.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtTimeEnd1.Location = New System.Drawing.Point(401, 53)
        Me.dtTimeEnd1.Name = "dtTimeEnd1"
        Me.dtTimeEnd1.ShowUpDown = True
        Me.dtTimeEnd1.Size = New System.Drawing.Size(101, 20)
        Me.dtTimeEnd1.TabIndex = 38
        Me.dtTimeEnd1.Value = New Date(2018, 2, 10, 18, 0, 0, 0)
        '
        'dtLunchEnd1
        '
        Me.dtLunchEnd1.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtLunchEnd1.Location = New System.Drawing.Point(294, 53)
        Me.dtLunchEnd1.Name = "dtLunchEnd1"
        Me.dtLunchEnd1.ShowUpDown = True
        Me.dtLunchEnd1.Size = New System.Drawing.Size(101, 20)
        Me.dtLunchEnd1.TabIndex = 37
        Me.dtLunchEnd1.Value = New Date(2018, 2, 10, 13, 0, 0, 0)
        '
        'dtLunchStart1
        '
        Me.dtLunchStart1.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtLunchStart1.Location = New System.Drawing.Point(187, 53)
        Me.dtLunchStart1.Name = "dtLunchStart1"
        Me.dtLunchStart1.ShowUpDown = True
        Me.dtLunchStart1.Size = New System.Drawing.Size(101, 20)
        Me.dtLunchStart1.TabIndex = 36
        Me.dtLunchStart1.Value = New Date(2018, 2, 10, 12, 0, 0, 0)
        '
        'dtTimeStart1
        '
        Me.dtTimeStart1.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtTimeStart1.Location = New System.Drawing.Point(80, 53)
        Me.dtTimeStart1.Name = "dtTimeStart1"
        Me.dtTimeStart1.ShowUpDown = True
        Me.dtTimeStart1.Size = New System.Drawing.Size(101, 20)
        Me.dtTimeStart1.TabIndex = 35
        Me.dtTimeStart1.Value = New Date(2018, 2, 10, 6, 0, 0, 0)
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(398, 37)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(52, 13)
        Me.Label17.TabIndex = 34
        Me.Label17.Text = "Time End"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(292, 37)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(59, 13)
        Me.Label16.TabIndex = 33
        Me.Label16.Text = "Lunch End"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(186, 37)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(62, 13)
        Me.Label15.TabIndex = 32
        Me.Label15.Text = "Lunch Start"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(80, 37)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(55, 13)
        Me.Label14.TabIndex = 31
        Me.Label14.Text = "Time Start"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(13, 180)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(49, 13)
        Me.Label13.TabIndex = 11
        Me.Label13.Text = "Saturday"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(13, 154)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(35, 13)
        Me.Label12.TabIndex = 5
        Me.Label12.Text = "Friday"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(13, 128)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(51, 13)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "Thursday"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(13, 109)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(64, 13)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = "Wednesday"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(13, 79)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(48, 13)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Tuesday"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(13, 53)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(45, 13)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Monday"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(20, 139)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(52, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Schedule"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'FormStudent
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(672, 443)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.studentImage)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.section)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.grade_level)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.student_id)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.last_name)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.middle_name)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.first_name)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label7)
        Me.MinimizeBox = False
        Me.Name = "FormStudent"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Student"
        CType(Me.studentImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents first_name As System.Windows.Forms.TextBox
    Friend WithEvents middle_name As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents last_name As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents student_id As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents section As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents grade_level As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents studentImage As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dtTimeEnd1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtLunchEnd1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtLunchStart1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtTimeStart1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents dtTimeEnd6 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtLunchEnd6 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtLunchStart6 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtTimeStart6 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtTimeEnd5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtLunchEnd5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtLunchStart5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtTimeStart5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtTimeEnd4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtLunchEnd4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtLunchStart4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtTimeStart4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtTimeEnd3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtLunchEnd3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtLunchStart3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtTimeStart3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtTimeEnd2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtLunchEnd2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtLunchStart2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtTimeStart2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
End Class
