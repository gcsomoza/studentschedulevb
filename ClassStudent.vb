﻿Imports MySql.Data.MySqlClient

Public Class Student
    Private connection As MySqlConnection

    Public id As Integer
    Public student_id As Integer
    Public bar_code As Integer
    Public first_name As String
    Public middle_name As String
    Public last_name As String
    Public grade_level As String
    Public section As String
    Public img() As Byte

    Public Sub New()
        connection = New MySqlConnection(GetConnectionString())
    End Sub

    Private Function OpenConnection() As Boolean
        Try
            connection.Open()
            Return True
        Catch ex As MySqlException
            MessageBox.Show(ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try
    End Function

    Public Function CloseConnection() As Boolean
        Try
            connection.Close()
            Return True
        Catch ex As MySqlException
            MessageBox.Show(ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try
    End Function

    Private Function LastInsertID() As Integer
        Dim last_insert_id As Integer

        Dim query As String
        query = "SELECT last_insert_id()"

        If OpenConnection() Then
            Dim cmd As New MySqlCommand()
            cmd.CommandText = query
            cmd.Connection = connection

            last_insert_id = Integer.Parse(cmd.ExecuteScalar())

            CloseConnection()
        End If

        Return last_insert_id
    End Function

    Public Function Insert() As Integer
        Dim query As String
        query = "INSERT INTO `student` " + _
        "(`student_id`,`bar_code`,`first_name`,`middle_name`,`last_name`," + _
        "`grade_level`,`section`) VALUES " + _
        "('" + student_id.ToString() + "','" + bar_code.ToString() + "','" + first_name + "','" + middle_name + _
        "','" + last_name + "','" + grade_level + "','" + section + "')"

        If OpenConnection() Then
            Dim cmd As New MySqlCommand()
            cmd.CommandText = query
            cmd.Connection = connection
            cmd.ExecuteNonQuery()

            CloseConnection()

            SaveImage()
        End If

        Return LastInsertID()
    End Function

    Public Sub Update()
        Dim query As String
        query = "UPDATE `student` SET `student_id` = '" + student_id.ToString() + "', `bar_code` = '" + bar_code.ToString() + "', `first_name` = '" + first_name + "', `middle_name` = '" + middle_name + "', " + _
        "`last_name` = '" + last_name + "', `grade_level` = '" + grade_level + "', `section` = '" + section + "' " + _
        "WHERE `id` = '" + id.ToString() + "'"

        If OpenConnection() Then
            Dim cmd As New MySqlCommand()
            cmd.CommandText = query
            cmd.Connection = connection
            cmd.ExecuteNonQuery()

            CloseConnection()

            SaveImage()
        End If
    End Sub

    Public Sub Delete()
        Dim query As String
        query = "DELETE FROM `student` WHERE `id` = '" + id.ToString() + "'"

        If OpenConnection() Then
            Dim cmd As New MySqlCommand()
            cmd.CommandText = query
            cmd.Connection = connection
            cmd.ExecuteNonQuery()

            CloseConnection()
        End If

        Me.DeleteSchedule()
    End Sub

    Public Sub SaveImage()
        If Me.id > 0 Then
            If img.Length > 0 Then
                OpenConnection()

                Dim query As String = "UPDATE `student` SET `image` = @img WHERE `id` = @id"
                Dim cmd As New MySqlCommand(query, Me.connection)
                cmd.Parameters.AddWithValue("@img", Me.img)
                cmd.Parameters.AddWithValue("@id", Me.id)
                cmd.ExecuteNonQuery()

                CloseConnection()
            End If
        End If
    End Sub

    Public Function DataGridViewQuery(ByVal condition As String) As String
        If Not condition = "" Then
            condition = " WHERE " + condition
        End If

        Dim query As String

        query = "SELECT id,student_id as `Student ID`,bar_code,first_name as `First Name`," + _
        "middle_name as `Middle Name`,last_name as `Last Name`, " + _
        "grade_level as `Grade Level`,section as `Section` FROM `student` " + _
        condition + _
        "ORDER BY `last_name`, `first_name`, `middle_name`"

        Return query
    End Function

    Public Function FetchQuery(ByVal condition As String) As String
        If Not condition = "" Then
            condition = " WHERE " + condition
        End If

        Dim query As String
        query = "SELECT * FROM `student` " + _
        condition + _
        "ORDER BY `last_name`, `first_name`, `middle_name`"

        Return query
    End Function

    Public Sub GetStudentData()
        If id > 0 Then
            Dim result As MySqlDataReader

            Dim query As String
            query = FetchQuery("`id` = '" + id.ToString() + "'")

            OpenConnection()

            Dim cmd As New MySqlCommand()
            cmd.CommandText = query
            cmd.Connection = connection

            result = cmd.ExecuteReader()

            While result.Read()
                student_id = result("student_id").ToString()
                bar_code = result("bar_code").ToString()
                first_name = result("first_name").ToString()
                middle_name = result("middle_name").ToString()
                last_name = result("last_name").ToString()
                grade_level = result("grade_level").ToString()
                middle_name = result("middle_name").ToString()
                img = DirectCast(result("image"), Byte())
            End While

            CloseConnection()
        End If
    End Sub

    Public Function GetID(ByVal barCode As String) As Integer
        Dim studentID As Integer

        If IsBarCodeExist(barCode) Then
            Dim query As String

            query = "SELECT `id` FROM `student` WHERE `bar_code` = '" + barCode + "'"

            Try
                If Me.OpenConnection() Then
                    Dim cmd As New MySqlCommand(query, Me.connection)

                    studentID = Integer.Parse(cmd.ExecuteScalar())

                    Me.CloseConnection()
                End If
            Catch ex As MySqlException

            End Try
        End If

        Return studentID
    End Function

    Public Function IsBarCodeExist(ByVal barCode As String) As Boolean
        Dim result As Boolean
        Dim query As String

        query = "SELECT COUNT(`id`) FROM `student` WHERE `bar_code` = '" + barCode + "'"

        Try
            If Me.OpenConnection() Then
                Dim cmd As New MySqlCommand(query, Me.connection)

                result = Integer.Parse(cmd.ExecuteScalar()) > 0

                Me.CloseConnection()
            End If
        Catch ex As MySqlException

        End Try

        Return result
    End Function

    Public Function GetTimeStart(ByVal studentID As Integer, ByVal dayOfWeek As Integer) As Integer
        Dim result As Integer
        Dim query As String

        'Current day of week is sunday.
        'In database sunday is 7 but in vb it is 0.
        ' Don't remove this code else it will raise error.
        If dayOfWeek = 0 Then
            dayOfWeek = 7
        End If

        If Not IsScheduleExist(dayOfWeek) Then
            Return result
        End If

        query = "SELECT `time_start` FROM `student_schedule` WHERE `student_id` = '" + studentID.ToString() + "' AND `weekday_id` = '" + dayOfWeek.ToString() + "'"

        Try
            If Me.OpenConnection() Then
                Dim cmd As New MySqlCommand(query, Me.connection)

                result = Integer.Parse(cmd.ExecuteScalar())

                Me.CloseConnection()
            End If
        Catch ex As MySqlException
            Me.CloseConnection()
        End Try

        Return result
    End Function

    Public Function GetTimeEnd(ByVal studentID As Integer, ByVal dayOfWeek As Integer) As Integer
        Dim result As Integer
        Dim query As String

        'Current day of week is sunday.
        'In database sunday is 7 but in vb it is 0.
        ' Don't remove this code else it will raise error.
        If dayOfWeek = 0 Then
            dayOfWeek = 7
        End If

        If Not IsScheduleExist(dayOfWeek) Then
            Return result
        End If

        query = "SELECT `time_end` FROM `student_schedule` WHERE `student_id` = '" + studentID.ToString() + "' AND `weekday_id` = '" + dayOfWeek.ToString() + "'"

        Try
            If Me.OpenConnection() Then
                Dim cmd As New MySqlCommand(query, Me.connection)

                result = Integer.Parse(cmd.ExecuteScalar())

                Me.CloseConnection()
            End If
        Catch ex As MySqlException
            Me.CloseConnection()
        End Try

        Return result
    End Function

    Public Function GetLunchStart(ByVal studentID As Integer, ByVal dayOfWeek As Integer) As Integer
        Dim result As Integer
        Dim query As String

        'Current day of week is sunday.
        'In database sunday is 7 but in vb it is 0.
        ' Don't remove this code else it will raise error.
        If dayOfWeek = 0 Then
            dayOfWeek = 7
        End If

        If Not IsScheduleExist(dayOfWeek) Then
            Return result
        End If

        query = "SELECT `lunch_start` FROM `student_schedule` WHERE `student_id` = '" + studentID.ToString() + "' AND `weekday_id` = '" + dayOfWeek.ToString() + "'"

        Try
            If Me.OpenConnection() Then
                Dim cmd As New MySqlCommand(query, Me.connection)

                result = Integer.Parse(cmd.ExecuteScalar())

                Me.CloseConnection()
            End If
        Catch ex As MySqlException
            Me.CloseConnection()
        End Try

        Return result
    End Function

    Public Function GetLunchEnd(ByVal studentID As Integer, ByVal dayOfWeek As Integer) As Integer
        Dim result As Integer
        Dim query As String

        'Current day of week is sunday.
        'In database sunday is 7 but in vb it is 0.
        ' Don't remove this code else it will raise error.
        If dayOfWeek = 0 Then
            dayOfWeek = 7
        End If

        If Not IsScheduleExist(dayOfWeek) Then
            Return result
        End If

        query = "SELECT `lunch_end` FROM `student_schedule` WHERE `student_id` = '" + studentID.ToString() + "' AND `weekday_id` = '" + dayOfWeek.ToString() + "'"

        Try
            If Me.OpenConnection() Then
                Dim cmd As New MySqlCommand(query, Me.connection)

                result = Integer.Parse(cmd.ExecuteScalar())

                Me.CloseConnection()
            End If
        Catch ex As MySqlException
            Me.CloseConnection()
        End Try

        Return result
    End Function

    Private Function IsScheduleExist(ByVal dayOfWeek As Integer) As Boolean
        Dim result As Boolean
        Dim query As String

        'Current day of week is sunday.
        'In database sunday is 7 but in vb it is 0.
        ' Don't remove this code else it will raise error.
        If dayOfWeek = 0 Then
            dayOfWeek = 7
        End If

        query = "SELECT COUNT(`id`) FROM `student_schedule` WHERE `student_id` = '" + Me.id.ToString() + "' AND `weekday_id` = '" + dayOfWeek.ToString() + "'"

        Try
            If Me.OpenConnection() Then
                Dim cmd As New MySqlCommand(query, Me.connection)

                result = Integer.Parse(cmd.ExecuteScalar()) > 0

                Me.CloseConnection()
            End If
        Catch ex As MySqlException
            Me.CloseConnection()
        End Try

        Return result
    End Function

    Public Sub SaveSchedule(ByVal dayOfWeek As Integer, ByVal timeStart As Integer, ByVal lunchStart As Integer, ByVal lunchEnd As Integer, ByVal timeEnd As Integer)
        Dim query As String

        'Current day of week is sunday.
        'In database sunday is 7 but in vb it is 0.
        ' Don't remove this code else it will raise error.
        If dayOfWeek = 0 Then
            dayOfWeek = 7
        End If

        If Not Me.IsScheduleExist(dayOfWeek) Then
            'Insert
            query = "INSERT INTO `student_schedule` (`student_id`, `weekday_id`, `time_start`, `lunch_start`, `lunch_end`, `time_end`) VALUES " + _
            "('" + Me.id.ToString() + "','" + dayOfWeek.ToString() + "','" + timeStart.ToString() + "','" + lunchStart.ToString() + "','" + lunchEnd.ToString() + "','" + timeEnd.ToString() + "')"
        Else
            'Update
            query = "UPDATE `student_schedule` SET `time_start` = '" + timeStart.ToString() + "', " + _
            "`lunch_start` = '" + lunchStart.ToString() + "', " + _
            "`lunch_end` = '" + lunchEnd.ToString() + "', " + _
            "`time_end` = '" + timeEnd.ToString() + "' " + _
            "WHERE `student_id` = '" + Me.id.ToString() + "' AND `weekday_id` = '" + dayOfWeek.ToString() + "'"
        End If

        If OpenConnection() Then
            Dim cmd As New MySqlCommand()
            cmd.CommandText = query
            cmd.Connection = connection
            cmd.ExecuteNonQuery()

            CloseConnection()
        End If
    End Sub

    Private Sub DeleteSchedule()
        Dim query As String
        query = "DELETE FROM `student_schedule` WHERE `student_id` = '" + Me.id.ToString() + "'"

        If OpenConnection() Then
            Dim cmd As New MySqlCommand()
            cmd.CommandText = query
            cmd.Connection = connection
            cmd.ExecuteNonQuery()

            CloseConnection()
        End If
    End Sub

    ' CloseConnection() needs to be called after 
    ' using the result of Fetch() function
    '
    ' result = Student.Fetch()
    '
    ' while (dataReader.Read()) {
    '   // Usage: dataReader["id"]
    ' }
    '
    ' result.Close()
    '
    ' Student.Close()
    Public Function Fetch(ByVal condition As String) As MySqlDataReader
        Dim result As MySqlDataReader

        Dim query As String
        query = FetchQuery(condition)

        OpenConnection()

        Dim cmd As New MySqlCommand()
        cmd.CommandText = query
        cmd.Connection = connection

        result = cmd.ExecuteReader()

        Return result
    End Function
End Class
