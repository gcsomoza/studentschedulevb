﻿Imports MySql.Data.MySqlClient

Public Class FormStudentList

    Public Sub LoadStudents(ByVal condition As String)
        Dim student As New Student
        Dim connection As New MySqlConnection(GetConnectionString())
        Dim table As New DataTable
        Dim adapter As New MySqlDataAdapter(student.DataGridViewQuery(condition), connection)

        adapter.Fill(table)

        dgvStudents.DataSource = table

        dgvStudents.Columns(0).Visible = False 'Hide database id in view
        dgvStudents.Columns(2).Visible = False 'Hide database bar_code in view
    End Sub

    Private Sub FormStudentList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadStudents("")
    End Sub

    Private Sub dgvStudents_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvStudents.CellDoubleClick
        Dim dgv As DataGridView = sender
        Dim id As Integer = Integer.Parse(dgv.CurrentRow.Cells(0).Value.ToString())

        Dim frm As New FormStudent()
        frm.id = id
        frm.frmCaller = Me
        frm.Show()
    End Sub

    Private Sub btnFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        Dim filter As String = txtFilter.Text
        Dim condition As String = ""
        If Not filter = "" Then
            Dim values() As String = filter.Split(" ")
            Dim n As Integer = values.Length - 1
            For i As Integer = 0 To n
                Dim op As String = " OR " 'operator
                If i = n Then
                    op = ""
                End If

                Dim value As String = values(i)
                condition += "(`last_name` LIKE '%" + value + "%' OR " + _
                "`first_name` LIKE '%" + value + "%' OR " + _
                "`middle_name` LIKE '%" + value + "%') OR " + _
                "`student_id` = '" + value + "'" + op
            Next
        End If

        LoadStudents(condition)
    End Sub

    Private Sub txtFilter_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtFilter.KeyUp
        If e.KeyCode = Keys.Enter Then
            btnFilter_Click(sender, e)
        End If
    End Sub

    Private Sub btnAddStudent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddStudent.Click
        Dim frm As New FormStudent()
        frm.frmCaller = Me
        frm.Show()
    End Sub
End Class