﻿Imports MySql.Data.MySqlClient
Imports System.IO

Public Class FormDatabaseConnection

    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        Dim connectionString As String
        connectionString = "SERVER=" + txtServer.Text + ";DATABASE=" + txtDatabase.Text + ";UID=" + txtUsername.Text + ";PASSWORD=" + txtPassword.Text + ";"

        Dim connection As New MySqlConnection(connectionString)

        Dim connectionSuccessful As Boolean

        Try
            connection.Open()
            connectionSuccessful = True

        Catch ex As MySqlException

            If ex.Number = 0 Then
                MessageBox.Show("Cannot connect to server. Contact administrator. " + ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Number = 1045 Then
                MessageBox.Show("Invalid username/password. Please try again.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

            connectionSuccessful = False

        End Try


        If connectionSuccessful Then

            Dim filePath As String
            filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\", "StudentScheduleConnectionString.txt")

            Using writer As New StreamWriter(filePath, False)
                writer.Write(connectionString)
            End Using

            Try
                connection.Close()
                MessageBox.Show("Connection successful!", "SUCCESS", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Catch ex As MySqlException
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If

    End Sub

    Private Sub btnShowConnectionString_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShowConnectionString.Click

        Dim connectionString As String = GetConnectionString()

        If connectionString = "" Then
            MessageBox.Show("Not yet connected to database. Contact administrator", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            MessageBox.Show(connectionString)
        End If
    End Sub
End Class
